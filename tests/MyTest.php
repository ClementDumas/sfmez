<?php
/**
 * Created by PhpStorm.
 * User: Clem
 * Date: 25/05/2020
 * Time: 14:07
 */

namespace App\Tests;


use App\Entity\Store;
use PHPUnit\Framework\TestCase;

class MyTest extends TestCase
{
    public function testAdd()
    {
        $store = new Store();
        $store->setAddress("6 Avenue Charles Floquet, 75007 Paris, France");

        $lat = $store->getLatitude();
        $expected = 48.856;
        // assert that your calculator added the numbers correctly!
        $this->assertEquals($expected, round($lat*1000)/1000);
    }
}
