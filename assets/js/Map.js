const Map = {
    markers: [],
    initMap() {
        if (document.getElementById("map-container")) {
            this.mapContainer = document.getElementById("map-container");
            this.mymap = L.map('map-container').setView([48.8534, 2.3488], 5);
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiY2xlbWR1bXMiLCJhIjoiY2pvdTYycGV2MDBiaTNrbWo1dnpoZXBzYyJ9.93A4hyh4aZzZC43cPbNdsg', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'your.mapbox.access.token'
            }).addTo(this.mymap);
            this.addMarkersInfos();
            this.addMarkers();
            this.markersClick();
        }
    },

    addMarkersInfos() {
        let stores = this.mapContainer.querySelectorAll('.store');
        stores.forEach((item) => {
            const lat = item.querySelector(".lat").innerHTML;
            const long = item.querySelector(".long").innerHTML;
            const name = item.querySelector(".name").innerHTML;
            const description = item.querySelector(".description").innerHTML;
            const city = item.querySelector(".city").innerHTML;
            const address = item.querySelector(".address").innerHTML;

            let openingHoursArray = [];
            const opening = item.querySelectorAll('.opening');
            opening.forEach(op => {
                let openingHour = {};
                openingHour.day = op.querySelector('.day').innerHTML;
                openingHour.open = op.querySelector('.open').innerHTML;
                openingHour.close = op.querySelector('.close').innerHTML;
                openingHoursArray.push(openingHour);
            });

            let marker = L.marker([lat, long]);
            let popuText =
                "<b>" + name + "</b>" +
                "<br>" + description + "" +
                "<br>" + city + "" +
                "<br>" + address +
                "<br><br><b>Opening hours :</b>";

            openingHoursArray.forEach(item=>{
               popuText += "<br><br>" + item.day;
               popuText += "<br>" + item.open;
               popuText += "<br>" + item.close;
            });
            marker.bindPopup(popuText);


            this.markers.push(marker);
        });
    },
    addMarkers() {
        this.markers.forEach((marker) => {
            marker.addTo(this.mymap);
        });
    },

    markersClick() {
        this.markers.forEach((marker) => {
            marker.addEventListener("click", () => {
                marker.openPopup();
            });
        });

    }
};
export default Map;
