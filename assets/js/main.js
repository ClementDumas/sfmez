import Map from './Map'
import Form from './Form'
const Main = {
    init(){
        Form.initOpeningHoursForm();
        Map.initMap();
    }

};
export default Main;
