const Form = {
    initOpeningHoursForm() {
        const collectionHolder = document.getElementById("opening_list");
        if (collectionHolder) {
            const newLinkLi = document.createElement("li");
            collectionHolder.appendChild(newLinkLi);

            const addItemButton = document.createElement("button");
            addItemButton.setAttribute("type", "button");
            addItemButton.classList.add("add_tag_link");
            addItemButton.classList.add("btn");
            addItemButton.classList.add("btn-primary");
            addItemButton.innerText = "Add opening time";
            newLinkLi.appendChild(addItemButton);

            let index = collectionHolder.querySelectorAll(".panel");
            index = index.length;
            collectionHolder.dataset.index = index;

            addItemButton.addEventListener("click", () => {
                this.addOpeningForm(collectionHolder, newLinkLi);
            });

            const panels = collectionHolder.querySelectorAll(".panel");
            panels.forEach(item => this.addTagFormDeleteLink(item));
        }

    },

    addOpeningForm(collectionHolder, newLinkLi) {
        const prototype = collectionHolder.getAttribute("data-prototype");

        let index = collectionHolder.getAttribute("data-index");
        let newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);
        let indexInt = parseInt(index);
        indexInt += 1;
        collectionHolder.dataset.index = indexInt;

        const panel = document.createElement("li");
        panel.classList.add("panel");

        panel.innerHTML = newForm;

        this.addTagFormDeleteLink(panel);
        collectionHolder.insertBefore(panel, newLinkLi);
    },

    addTagFormDeleteLink(item) {
        const removeItemButton = document.createElement("button");
        removeItemButton.setAttribute("type", "button");
        removeItemButton.classList.add("add_tag_link");
        removeItemButton.classList.add("btn");
        removeItemButton.classList.add("btn-danger");
        removeItemButton.innerText = "Remove opening time";
        item.appendChild(removeItemButton);

        removeItemButton.addEventListener("click", () => {
            item.parentNode.removeChild(item)
        })
    }
};

export default Form;
