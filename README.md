How to install :

Clone project :
git clone https://gitlab.com/ClementDumas/sfmez.git
cd sfmez

Install dependencies :
npm install
composer install

Webpack watch :
npm run watch

Symfony server :
symfony server:start

Change .env database URL

php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate
 
php bin/console doctrine:fixtures:load
