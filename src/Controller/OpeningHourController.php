<?php

namespace App\Controller;

use App\Entity\OpeningHour;
use App\Form\OpeningHourType;
use App\Repository\OpeningHourRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/openinghour")
 */
class OpeningHourController extends AbstractController
{
    /**
     * @Route("/", name="opening_hour_index", methods={"GET"})
     */
    public function index(OpeningHourRepository $openingHourRepository): Response
    {
        return $this->render('opening_hour/index.html.twig', [
            'opening_hours' => $openingHourRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="opening_hour_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $openingHour = new OpeningHour();
        $form = $this->createForm(OpeningHourType::class, $openingHour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($openingHour);
            $entityManager->flush();

            return $this->redirectToRoute('opening_hour_index');
        }

        return $this->render('opening_hour/new.html.twig', [
            'opening_hour' => $openingHour,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="opening_hour_show", methods={"GET"})
     */
    public function show(OpeningHour $openingHour): Response
    {
        return $this->render('opening_hour/show.html.twig', [
            'opening_hour' => $openingHour,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="opening_hour_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OpeningHour $openingHour): Response
    {
        $form = $this->createForm(OpeningHourType::class, $openingHour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('opening_hour_index');
        }

        return $this->render('opening_hour/edit.html.twig', [
            'opening_hour' => $openingHour,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="opening_hour_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OpeningHour $openingHour): Response
    {
        if ($this->isCsrfTokenValid('delete'.$openingHour->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($openingHour);
            $entityManager->flush();
        }

        return $this->redirectToRoute('opening_hour_index');
    }
}
