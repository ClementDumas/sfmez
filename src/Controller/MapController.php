<?php
/**
 * Created by PhpStorm.
 * User: Clem
 * Date: 23/05/2020
 * Time: 22:21
 */

namespace App\Controller;


use App\Repository\StoreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/map")
 */
class MapController extends AbstractController
{
    /**
     * @Route("/", name="map_index", methods={"GET"})
     */
    public function index(StoreRepository $storeRepository)
    {
        return $this->render('map/index.html.twig', [
            'stores' => $storeRepository->findAll(),
        ]);
    }
}
