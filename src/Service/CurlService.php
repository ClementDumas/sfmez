<?php
/**
 * Created by PhpStorm.
 * User: Clem
 * Date: 22/05/2020
 * Time: 20:07
 */

namespace App\Service;


use Symfony\Component\HttpClient\CurlHttpClient;

class CurlService
{
    private $curlClient;

    public function __construct()
    {
        $this->curlClient = new CurlHttpClient();
    }

    function getCoordByAddress($address)
    {
        $response = $this->curlClient->request('GET', 'https://api-adresse.data.gouv.fr/search/?q=' . $address . '');
        // trying to get the response contents will block the execution until
        // the full response contents are received
        $contents = $response->toArray();
        $latitude = $contents["features"][0]["geometry"]["coordinates"][1];
        $longitude = $contents["features"][0]["geometry"]["coordinates"][0];
        $coord = [$latitude, $longitude];
        return $coord;
    }
}
