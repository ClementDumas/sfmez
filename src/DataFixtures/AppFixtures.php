<?php
/**
 * Created by PhpStorm.
 * User: Clem
 * Date: 21/05/2020
 * Time: 23:28
 */

namespace App\DataFixtures;

use App\Entity\Day;
use App\Entity\OpeningHour;
use App\Entity\Store;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     */
    public function load(ObjectManager $manager)
    {
        /****DAYS****/
        $monday = new Day();
        $monday->setName("monday");
        $manager->persist($monday);
        $tuesday = new Day();
        $tuesday->setName("tuesday");
        $manager->persist($tuesday);
        $wed = new Day();
        $wed->setName("wednesday");
        $manager->persist($wed);
        $thursday = new Day();
        $thursday->setName("thursday");
        $manager->persist($thursday);
        $friday = new Day();
        $friday->setName("friday");
        $manager->persist($friday);
        $saturday = new Day();
        $saturday->setName("saturday");
        $manager->persist($saturday);
        $sunday = new Day();
        $sunday->setName("sunday");
        $manager->persist($sunday);

        /***OPENING HOURS***/
        $op1 = new OpeningHour();
        $op1->setDay($monday);
        $op1->setStartTime(new \DateTime());
        $op1->setEndTime(new \DateTime());


        /***STORE***/
        $store1 = new Store();
        $store1->setName("Les 7 Royaumes");
        $store1->setCity("Grenoble");
        $store1->setAddress("5 Rue Lafayette, 38000 Grenoble");
        $store1->setDescription("Magasin de jeux de société");
        $store1->setPostalCode("38000");
        $store1->addOpeningHour($op1);

        $store2 = new Store();
        $store2->setName("Primark");
        $store2->setCity("Toulouse");
        $store2->setAddress("41 Rue de Rémusat, 31000 Toulouse");
        $store2->setDescription("Grand magasin");
        $store2->setPostalCode("31000");
        $store2->addOpeningHour($op1);

        $store3 = new Store();
        $store3->setName("Friterie Meunier");
        $store3->setCity("Lille");
        $store3->setAddress("56 Place Charles de Gaulle, 59000 Lille");
        $store3->setDescription("Friterie");
        $store3->setPostalCode("59000");
        $store3->addOpeningHour($op1);

        $manager->persist($store1);
        $manager->persist($store2);
        $manager->persist($store3);

        $manager->flush();
    }
}
