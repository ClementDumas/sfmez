<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200521204536 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE day (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opening_hour (id INT AUTO_INCREMENT NOT NULL, start_time TIME NOT NULL, end_time TIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opening_hour_store (opening_hour_id INT NOT NULL, store_id INT NOT NULL, INDEX IDX_855C2BAC81F9D579 (opening_hour_id), INDEX IDX_855C2BACB092A811 (store_id), PRIMARY KEY(opening_hour_id, store_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opening_hour_day (opening_hour_id INT NOT NULL, day_id INT NOT NULL, INDEX IDX_A72C714E81F9D579 (opening_hour_id), INDEX IDX_A72C714E9C24126 (day_id), PRIMARY KEY(opening_hour_id, day_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE store (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, postal_code INT NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE opening_hour_store ADD CONSTRAINT FK_855C2BAC81F9D579 FOREIGN KEY (opening_hour_id) REFERENCES opening_hour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE opening_hour_store ADD CONSTRAINT FK_855C2BACB092A811 FOREIGN KEY (store_id) REFERENCES store (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE opening_hour_day ADD CONSTRAINT FK_A72C714E81F9D579 FOREIGN KEY (opening_hour_id) REFERENCES opening_hour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE opening_hour_day ADD CONSTRAINT FK_A72C714E9C24126 FOREIGN KEY (day_id) REFERENCES day (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opening_hour_day DROP FOREIGN KEY FK_A72C714E9C24126');
        $this->addSql('ALTER TABLE opening_hour_store DROP FOREIGN KEY FK_855C2BAC81F9D579');
        $this->addSql('ALTER TABLE opening_hour_day DROP FOREIGN KEY FK_A72C714E81F9D579');
        $this->addSql('ALTER TABLE opening_hour_store DROP FOREIGN KEY FK_855C2BACB092A811');
        $this->addSql('DROP TABLE day');
        $this->addSql('DROP TABLE opening_hour');
        $this->addSql('DROP TABLE opening_hour_store');
        $this->addSql('DROP TABLE opening_hour_day');
        $this->addSql('DROP TABLE store');
    }
}
