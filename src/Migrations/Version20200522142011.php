<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200522142011 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opening_hour ADD day_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE opening_hour ADD CONSTRAINT FK_969BD7659C24126 FOREIGN KEY (day_id) REFERENCES day (id)');
        $this->addSql('CREATE INDEX IDX_969BD7659C24126 ON opening_hour (day_id)');
        $this->addSql('ALTER TABLE opening_hour_store ADD CONSTRAINT FK_855C2BAC81F9D579 FOREIGN KEY (opening_hour_id) REFERENCES opening_hour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE opening_hour_store ADD CONSTRAINT FK_855C2BACB092A811 FOREIGN KEY (store_id) REFERENCES store (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE opening_hour DROP FOREIGN KEY FK_969BD7659C24126');
        $this->addSql('DROP INDEX IDX_969BD7659C24126 ON opening_hour');
        $this->addSql('ALTER TABLE opening_hour DROP day_id');
        $this->addSql('ALTER TABLE opening_hour_store DROP FOREIGN KEY FK_855C2BAC81F9D579');
        $this->addSql('ALTER TABLE opening_hour_store DROP FOREIGN KEY FK_855C2BACB092A811');
    }
}
