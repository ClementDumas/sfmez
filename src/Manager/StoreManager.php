<?php
/**
 * Created by PhpStorm.
 * User: Clem
 * Date: 25/05/2020
 * Time: 11:49
 */

namespace App\Manager;


use App\Service\CurlService;

class StoreManager
{
    private $curlService;

    public function __construct()
    {
        $this->curlService = new CurlService();
    }

    public function getCoordByAddress($address)
    {
        $coord = $this->curlService->getCoordByAddress($address);
        return $coord;
    }
}
